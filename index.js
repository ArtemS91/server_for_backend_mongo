const http = require('http');
const getAllUsers = require("./db/index");

http.createServer(async (req,res)=> {
    // THIS IS FOR CORS ERRORS
    res.setHeader('Access-Control-Allow-Origin', '*'); /* @dev First, read about security */
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Max-Age', 2592000); // 30 days
    if(req.url == '/'){
        try {
            res.writeHead(200, {'Content-Type':'application/json'});
            const dataset = await getAllUsers(); // here we get the string json
            res.write(JSON.stringify(dataset)); // whoever requests will get the string json as response
        }
        finally {
            res.end(); // must end response. DONT FOGET
        }
    }
}).listen('https://server-for-backend-mongo.vercel.app/');
// const express = require("express");
// require("./db/index");
// const app = express();
// const cors = require("cors");


// app.use(cors());
// app.use(app.json());

// const array=[
//     {
//         id:1,
//         item:"Artem"
//     }
// ]
// app.get("/", async function (req, res) {
//   const data = await getAllUsers();
// //   res.writeHead(200, {
// //     "Content-type": "application/json",
// //   });
//  res.json(array);
// });
// app.use(routesUser.routes()).use(routesUser.allowedMethods());

// app.listen('https://server-for-backend-mongo.vercel.app/');

console.log("Server is running on port 3001");
